#include "distance_monitoring.h"
#include "hcsr04.h"

#define DISTANCE_TICKER_TIME 0.020 // 20ms

HCSR04  senzor_dreapta(PTD0,PTD2);
HCSR04  senzor_stanga(PTA13,PTD5);
unsigned int calculated_distance_left=9999, calculated_distance_right=9999;
Ticker distance_monitor_ticker;

void initialize_distance_monitorisation()
{
    senzor_stanga.start(); 
    senzor_dreapta.start();
    distance_monitor_ticker.attach(&start_save_distance_calculation, DISTANCE_TICKER_TIME);
        
}

void start_save_distance_calculation()
{
    if(senzor_stanga.get_flag() == 1)
    {
        calculated_distance_left=senzor_stanga.get_dist_cm();
        senzor_stanga.start();
    }   
    if(senzor_dreapta.get_flag() == 1)
    {
        calculated_distance_right=senzor_dreapta.get_dist_cm();
        senzor_dreapta.start();
    }  
}