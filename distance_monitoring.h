
#ifndef distance_monitoring_f
#define distance_monitoring_f

extern unsigned int calculated_distance_left, calculated_distance_right;

void initialize_distance_monitorisation();
void start_save_distance_calculation();

#endif