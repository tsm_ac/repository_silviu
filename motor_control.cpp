#include "motor_control.h"
#include "TFC.h"

float acc_max = 0.40;
float acc_min = 0.35;
float acc_left= acc_min, acc_right = acc_min;
int time_to_acc = 10;
float steer =0 ;

void calculate_motors_speed()
{
    float acc_maximum=0;
    /* get the maximum speed of the two wheels */
     if (steer>0)
    {
     acc_maximum = acc_right;
     }
    else{
        acc_maximum = acc_left;
     }

     if(acc_maximum < acc_min)
     {
         acc_maximum = acc_min;
     }
     /******************************************/
     
     if ((steer >= -0.09 && steer <= 0.09))
    {
        if(time_to_acc > 1){
            time_to_acc -= 1;
            acc_left = acc_maximum + (acc_max - acc_maximum)/time_to_acc;
            acc_right = acc_left; 
        }
        else{
            acc_left = acc_max ;
            acc_right = acc_max ;    
        }
    }
    else if (steer >0 ){
     time_to_acc = 10;
     acc_left = acc_maximum * (1 - 7*steer/10);
     acc_right = acc_maximum * (1- 2*steer/10);  
    }
    else{
     time_to_acc = 10;
     acc_right = acc_maximum * (1+ 7*steer/10);   
     acc_left = acc_maximum * (1 + 2*steer/10);  
    }

}

void update_servo_command()
{
    TFC_SetServo(0, -steer);
}

void stop()
{
    TFC_SetMotorPWM(0.2, 0.2);
    wait(0.5);
    TFC_SetMotorPWM(0, 0);
    wait(1000);
}
