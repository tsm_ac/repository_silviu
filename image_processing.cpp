
#include "TFC.h"
#include "image_processing.h"
#include "tsm_ac.h"

/* defines */
//intensitatea luminii mare, creste diff_tresh: 30
#define DIFF_THRESH     70
#define FRAME_DROP      100
#define MEDIE_THRESH    20//de completat

#define max_pixel_intensity 3500
#define min_pixel_intensity 2500

extern float steer;

/* variables */
int MEAN_OF_TRACK = 58;
int pixels_save[4]={0};
int x_old = 0;
int middle_pixel_intensity = 0;
int max_pixel_calibration = 0;

float medie_old = 0;

uint16_t camera_calib_params[128];

uint16_t current_camera_frame[128];
/* functions */


void adapt_exposure_time()
{
     // 3.2ms <-> 32ms to take an img
    if(middle_pixel_intensity >max_pixel_intensity && light_intensity_adapter > 2000)
    {
        light_intensity_adapter -= 200;
    }
    else if (middle_pixel_intensity < min_pixel_intensity && light_intensity_adapter < 20000)
    {
        light_intensity_adapter += 200;
    }    

}


void initial_exposure_calibration()
{
    TFC_LineScanImageReady = 0;
    
    while(TFC_LineScanImageReady == 0);
    TFC_LineScanImageReady= 0;

    middle_pixel_intensity = TFC_LineScanImage0[MEAN_OF_TRACK];

    while (middle_pixel_intensity < min_pixel_intensity || middle_pixel_intensity > max_pixel_intensity)
    {
        adapt_exposure_time();

        while(TFC_LineScanImageReady == 0);
        TFC_LineScanImageReady= 0;

        middle_pixel_intensity = TFC_LineScanImage0[MEAN_OF_TRACK];
    }
}

/*
 * Functie care niveleaza semnalul de la camera.
 * La faza de configurare a masinii, camera trebuie sa vada doar traseul alb.
 * In acest moment ne salvam un vector de calibrare, care niveleaza datele primite.
 */
void read_touch_for_camera_calibration()
{
    float x = 0.0;
    rled = 0;
    gled= 0;
    bled = 0;
    while(x < 0.001){
    x = tsi_button.readPercentage();
    wait_ms(20);
    }
    
    initial_exposure_calibration();

    TFC_LineScanImageReady = 0;
    
    while(TFC_LineScanImageReady == 0);
    TFC_LineScanImageReady= 0;


    /*      adun 5 sampleuri         */
    for(int i=0;i<128;i++)
    {
        current_camera_frame[i] = TFC_LineScanImage0[i];
    }
    for(int j=0;j<4;j++)
    {
        while(TFC_LineScanImageReady == 0);
        TFC_LineScanImageReady = 0;
        for(int i=0;i<128;i++)
        {
            current_camera_frame[i] += TFC_LineScanImage0[i];
        }
    }
    for(int i=0;i<128;i++)
    {
        camera_calib_params[i] = (current_camera_frame[i] / 5)+1;
    }
    /*******************************/

    for(int i = 0; i < 128; i++)
    {
        if (max_pixel_calibration < camera_calib_params[i])
        {
            max_pixel_calibration = camera_calib_params[i];
        }                    
        
    }
    
    for(int i=0;i<128;i++)
    {
        camera_calib_params[i] = max_pixel_calibration - camera_calib_params[i];
    }

    //PC.printf("<");
    for (int i=0;i<127;i++)
    {
        //PC.printf("%d,",camera_calib_params[i]);
    }
    
    //PC.printf("%d>\r\n",camera_calib_params[127]);
    wait_ms(1000);
    rled = 0;
    gled= 1;
    bled = 0;
}

/*
 * Print the camera pixel to Serial output
 */
void print_camera_frame()
{
    PC.printf("<");
    for (int i=0;i<127;i++)
    {
        PC.printf("%d,",current_camera_frame[i]);
    }
    PC.printf("%d>\r\n",current_camera_frame[127]);
}

int right_old = 120, left_old = 8;
int detected_on_left =0, detected_on_right = 0, detected_on_right_old, detected_on_left_old;
extern float steer;

int obstacle_detection(int right, int left)
{
    int ret_val = 0;

    if (steer >= -0.4 && steer < 0.4 )
    {
        //        if(right > 80 && left < 40){
            if( right_old < 110 && right == -1 && left < 40 && left != -1)
            {
                detected_on_right ++;
            }
            else if(left_old > 13 && left == -1 && right > 80 && right != -1)
            {
                detected_on_left ++;
            }
            if (right != -1) right_old = right;
            if (left != -1) left_old = left;

            if (detected_on_left > 0 && detected_on_right > 0)
            {
                detected_on_left = 0;
                detected_on_right = 0;
                ret_val = 0;
            }

            if (detected_on_left == 4)
            {
                ret_val = 1;
            }
            else if(detected_on_right == 4)
            {
                ret_val = 2;
            }
            else if((detected_on_right_old +1 == detected_on_right && detected_on_right > 0 && detected_on_right < 4 && detected_on_left == 0) || (detected_on_left < 4 && detected_on_right == 0 && detected_on_left > 0 && detected_on_left== detected_on_left_old + 1))
            {
                ret_val = 55;
            }
            
    }
    else
    {
        detected_on_right = 0;
        detected_on_left = 0;
    }
    //PC.printf("r=%d\tro=%d\tl=%d\tlo=%d\tdetr=%d\tdetl=%d\tretv=%d\r\n",right,right_old,left,left_old,detected_on_right,detected_on_left,ret_val);
    detected_on_right_old = detected_on_right;
    detected_on_left_old = detected_on_left;
    return ret_val;
}


Timeout obstacle_passed_timeup;
int obstacle_side;
int flag_start_detect_obstacle = 1;

void reset_obstacle_flag()
{
    obstacle_side = 0;  
    right_old = 120;
    left_old = 8; 
    detected_on_right = 0;
    detected_on_left = 0;
    flag_start_detect_obstacle = 1;
}


/*
 * Finds the margins of the track and returns its center.
 */
int find_middle_error()
{
    int i;
    int left, right;
    int x_new = 0;
    int max_pixel=0;
    
    for(int i = 0; i < 128; i++)
    {
        current_camera_frame[i] = (TFC_LineScanImage0[i]);
        if (max_pixel < current_camera_frame[i])
        {
            max_pixel = current_camera_frame[i];
        }                    
        
    }
    middle_pixel_intensity = max_pixel;
    adapt_exposure_time();

    /* astea as vrea sa vad cum arata pe grafic */
    for(i=0;i<128;i++)
    {
        // adaug proportia care lipseste pixelului (pe baza calibrarii)
        current_camera_frame[i] = current_camera_frame[i] + (camera_calib_params[i]* max_pixel)/max_pixel_calibration;
    }
    //print_camera_frame();
    //adapt_speed_for_lines();

    right = -1;
    for(i = x_old + MEAN_OF_TRACK; i < 127; i++) {
        
        if ( current_camera_frame[i-1] * DIFF_THRESH > current_camera_frame[i+1] * 100 ) {
            
                right = i;
                break;
        }
    }
            
    left = -1;
    for(i = x_old +MEAN_OF_TRACK; i > 1; i--) {
        if (  current_camera_frame[i+1] * DIFF_THRESH > current_camera_frame[i-1] * 100) {
          
                left = i;
                break;
        }
    }     

    if (right == -1 ) right  = 120;
    if (left == -1) left = 8;



    x_new = ((left + right) / 2 ) - MEAN_OF_TRACK;
    x_old = x_new;  
    
    return x_new;
}



/*
 * This function detects the obstacle that may be placed on the track.
 */

int guide_and_watch_obstacle()
{
    int i;
    int left, right;
    int x_new = 0;
    int max_pixel=0;

    // citesc camera
    for(int i = 0; i < 128; i++)
    {
        current_camera_frame[i] = (TFC_LineScanImage0[i]);
        if (max_pixel < current_camera_frame[i])
        {
            max_pixel = current_camera_frame[i];
        }                    
        
    }
    middle_pixel_intensity = max_pixel;
    adapt_exposure_time();

    for(i=0;i<128;i++)
    {
        // adaug proportia care lipseste pixelului (pe baza calibrarii)
        current_camera_frame[i] = current_camera_frame[i] + (camera_calib_params[i]* max_pixel)/max_pixel_calibration;
    }
    //print_camera_frame();
    //adapt_speed_for_lines();

    right = -1;
    for(i = x_old + MEAN_OF_TRACK; i < 127; i++) {
        
        if ( current_camera_frame[i-1] * DIFF_THRESH > current_camera_frame[i+1] * 100 ) {
            
                right = i;
                break;
        }
    }
            
    left = -1;
    for(i = x_old +MEAN_OF_TRACK; i > 1; i--) {
        if (  current_camera_frame[i+1] * DIFF_THRESH > current_camera_frame[i-1] * 100) {
          
                left = i;
                break;
        }
    }     

    if (obstacle_side == 0 && flag_start_detect_obstacle)
    {
        obstacle_side = obstacle_detection(right, left); 
    }
    else if(flag_start_detect_obstacle == 1)
    {
        obstacle_passed_timeup.attach(&reset_obstacle_flag, 0.3);
        flag_start_detect_obstacle = 0;
    }

    if (right == -1 ) right  = 120;
    if (left == -1) left = 8;

    if (obstacle_side == 55)
    {
        obstacle_side = 0;
        return x_old;
    }

    x_new = ((left + right) / 2 ) - MEAN_OF_TRACK;
    x_old = x_new;  
    
    if (obstacle_side == 1)
    {
        //PC.printf("OBST 1\n"); // dreapta
        x_new = 20 ;
    }
    else if (obstacle_side == 2)
    {
        //PC.printf("OBST 2\n"); // stanga
        x_new = -16;

    }
    
    return x_new;
}



uint16_t old_camera_frame[128];
int nr_of_lines = 0, old_nr_of_lines;

int process_image_speed_zone()
{
    nr_of_lines = 0;
    int ret_val = 0;

    int max_pixel=0;
    int min_pixel = 10000;
    for(int i = 0; i < 128; i++)
    {
        if (max_pixel < current_camera_frame[i])
        {
            max_pixel = current_camera_frame[i];
        }       
        if (min_pixel > current_camera_frame[i])
        {
            min_pixel = current_camera_frame[i];
        }             
        
    }
    int pixel_level = max_pixel/ 2;

    for(int i = 0; i < 128; i++)
    {
        if (current_camera_frame[i] > pixel_level)
            current_camera_frame[i] = max_pixel;
        else
            current_camera_frame[i] = min_pixel;      
        
    }


    for (int i=1;i<110;i++)
    {
        if(current_camera_frame[i-1] * DIFF_THRESH > current_camera_frame[i+1] * 100)
        {
            nr_of_lines ++;
        }
    }

    if (nr_of_lines <= 3)
    {
        ret_val = 0;
    }
    else if (nr_of_lines == 5 && old_nr_of_lines == nr_of_lines)
    {
        ret_val = nr_of_lines;
    }
    else if (nr_of_lines == 6 && old_nr_of_lines == nr_of_lines)
    {
        ret_val = nr_of_lines;
    }
    else if (nr_of_lines >= 4)
    {
        ret_val = 1;
    }
    //PC.printf("NR LINII = %d\t%d\n", nr_of_lines, old_nr_of_lines);
    //print_camera_frame();
    old_nr_of_lines = nr_of_lines;
    return ret_val;
}

int speed_zone_detected_mayb = 0;
int speed_zone_choose_mayb = 0;
Timeout speed_zone_passed_timeup;
int search_speed_zone_flag = 1;
void reset_speed_zone_flag()
{
    search_speed_zone_flag = 1;
    speed_zone_detected_mayb = 0;
}

int detect_speed_zone()
{
    int i;
    int left, right;
    int x_new = 0;
    int max_pixel=0;
    for(int i = 0; i < 128; i++)
    {
        current_camera_frame[i] = (TFC_LineScanImage0[i]) & (0xFFF0);
        if (max_pixel < current_camera_frame[i])
        {
            max_pixel = current_camera_frame[i];
        }                    
        
    }
    middle_pixel_intensity = max_pixel;
    adapt_exposure_time();

    /* astea as vrea sa vad cum arata pe grafic */
    for(i=0;i<128;i++)
    {
        // adaug proportia care lipseste pixelului (pe baza calibrarii)
        current_camera_frame[i] = current_camera_frame[i] + (camera_calib_params[i]* max_pixel)/max_pixel_calibration;
    }
    print_camera_frame();

    if (search_speed_zone_flag == 1){
        speed_zone_detected_mayb = process_image_speed_zone();
    }
    right = -1;
    for(i = x_old + MEAN_OF_TRACK; i < 127; i++) {
        
        if ( current_camera_frame[i-1] * DIFF_THRESH > current_camera_frame[i+1] * 100 ) {
            
                right = i;
                break;
        }
    }
            
    left = -1;
    for(i = x_old +MEAN_OF_TRACK; i > 1; i--) {
        if (  current_camera_frame[i+1] * DIFF_THRESH > current_camera_frame[i-1] * 100) {
          
                left = i;
                break;
        }
    }     

    if (right == -1 ) right  = 120;
    if (left == -1) left = 8;

    if ( speed_zone_detected_mayb == 0)
    {
        x_new = ((left + right) / 2 ) - MEAN_OF_TRACK;
        x_old = x_new;  
    }
    else
    {
        x_new = 0;
    }

    if(speed_zone_detected_mayb == 6 || speed_zone_detected_mayb == 5)
    {
        speed_zone_choose_mayb = speed_zone_detected_mayb;
        speed_zone_detected_mayb = 1;
        search_speed_zone_flag = 0;
        speed_zone_passed_timeup.attach(&reset_speed_zone_flag, 0.7);
    }
    return x_new;
}